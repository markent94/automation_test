import signal
import sys
import threading
from StateMachine.state_machine import StateMachine
from Config.config import *
import time

# Define a flag that will indicate when the simulation should end
end_simulation = False

# Define the function that the timer will run
def end_simulation_after_timeout():
    global end_simulation
    time.sleep(SIMULATION_LENGTH)  # SIMULATION_LENGTH should be defined in seconds in your config
    end_simulation = True

def signal_handler(sig, frame):
    print(f"Signal {sig} received: Exiting gracefully")
    # Set a flag or use other methods to stop your threads and clean up resources
    # For example: stop_threads.set()
    sys.exit(0)

# Register the signal handlers
signal.signal(signal.SIGINT, signal_handler)  # Handler for CTRL+C

# Start the timer thread
timer_thread = threading.Thread(target=end_simulation_after_timeout)
timer_thread.start()

# Initialize
state_machine = StateMachine()
actions = 0

# Main loop
while not end_simulation:
    state_machine.update_state()
    state_machine.perform_action()

# Wait for the timer thread to finish if it hasn't already
timer_thread.join()
print("Done")
