import sys
import threading
import random
import time
import Actions.action_random as ActionRandom
import Actions.action_utility as ActionUtility
from Actions.action_construction import ActionConstruction
from Actions.action_craft import ActionCraft
from Actions.action_combat import ActionCombat
from enum import Enum
import Utility.cursor as Cursor
import Utility.ui_helper as UI
import Utility.keyboard  as Keyboard
from Config.config import *

class StateMachine:
    def __init__(self):  
        UI.set_focus_to_game()
        self.reset()
       
    def start_break_timer(self):
        # Define how long to wait before the break (in seconds)
        self.break_timer = threading.Thread(target=self.trigger_break)
        self.break_timer.start()
        print(f"Time until break: {self.remaining_time_until_break()} seconds")

    def trigger_break(self):
        break_time = random.randint(*ACTIVE_DURATION)
        self.scheduled_break_time = time.time() + break_time
        time.sleep(break_time)
        self.break_flag = True

    def remaining_time_until_break(self):
        # Calculate the remaining time until the break
        return max(int(self.scheduled_break_time - time.time()), 0)
    
    def reset(self):
        self.break_flag = False
        self.break_timer = self.start_break_timer()

        self.actions_count = 0
        self.fail_count = 0
        
        if MAIN_STATE == STATE.CRAFTING:
            self.state = STATE.CRAFTING
            self.action = ActionCraft()
        elif MAIN_STATE == STATE.CONSTRUCTING:
            self.state = STATE.CONSTRUCTING
            self.action = ActionConstruction()
        elif MAIN_STATE == STATE.ALCHING:
            self.state = STATE.ALCHING
        elif MAIN_STATE == STATE.ATTACKING:
            self.state = STATE.ATTACKING
            self.action = ActionCombat()
        
    def update_state(self):
        # Print remaining time every 25 actions
        if self.actions_count % 25 == 0:
            print(f"Time until break: {self.remaining_time_until_break()} seconds")

        if self.fail_count >= RESET_THRESHOLD:
            print("RESET_THRESHOLD REACHED")
            self.state = STATE.BREAK
            return
        
        if self.fail_count >= EXIT_THRESHOLD:
            print("EXIT_THRESHOLD REACHED")
            sys.exit(0)

        if self.break_flag:
            print("BREAK FLAG REACHED")
            self.state = STATE.BREAK
            return
            
    def perform_action(self):
        if self.run():
            self.fail_count = 0
        else:
            self.fail_count += 1
        self.actions_count += 0

    def run(self):
        action = run(ACTION.RANDOM)
        if self.state == STATE.IDLE:
            time.sleep(1)
        elif self.state == STATE.BREAK:
            action = run(ACTION.BREAK)
            if action:
                self.reset()     
        elif self.state == STATE.ALCHING:
            action = run(ACTION.ALCH)
        elif self.state == STATE.CONSTRUCTING:
            action = self.action.perform_action()
        elif self.state == STATE.CRAFTING:
            action = self.action.perform_action()
        elif self.state == STATE.ATTACKING:
            action = self.action.perform_action()
        return action


                