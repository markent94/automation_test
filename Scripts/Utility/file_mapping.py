from enum import Enum

class Color(Enum):
    Yellow = "#FFB6BA21"
    Purple = "#FF8E80FF"
    Blue = "#FF2600FF"
    Green = "#FFC3C573"
    Pink = "#FFFF00E7"

class Button(Enum):
    Account = "Images/Button/Account.PNG"
    Armor = "Images/Button/Armor.PNG"
    Bag = "Images/Button/Bag.PNG"
    Combat = "Images/Button/Combat.PNG"
    DepositBag = "Images/Button/DepositBag.PNG"
    Emotes = "Images/Button/Emotes.PNG"
    Exit = "Images/Button/Exit.PNG"
    Friends = "Images/Button/Friends.PNG"
    Grouping = "Images/Button/Grouping.PNG"
    HighAlchemy = "Images/Button/HighAlchemy.PNG"
    Logout = "Images/Button/Logout.PNG"
    Magic = "Images/Button/Magic.PNG"
    Music = "Images/Button/Music.PNG"
    Play = "Images/Button/Play.PNG"
    PlayNow = "Images/Button/PlayNow.PNG"
    Prayer = "Images/Button/Prayer.PNG"
    Quests = "Images/Button/Quests.PNG"
    QuickPray = "Images/Button/QuickPray.PNG"
    Settings = "Images/Button/Settings.PNG"
    Skills = "Images/Button/Skills.PNG"

class Environment(Enum):
    TileMarker = "Images/Environment/TileMarker.PNG"

class Interaction(Enum):
    Attack = "Images/Interaction/Attack.PNG"
    Build = "Images/Interaction/Build.PNG"
    BuildMode = "Images/Interaction/BuildMode.PNG"
    EnterPortal = "Images/Interaction/EnterPortal.PNG"
    Remove = "Images/Interaction/Remove.PNG"
    Talk = "Images/Interaction/Talk.PNG"
    Fire = "Images/Interaction/Fire.PNG"

class Item(Enum):
    AbsorbPotion = "Images/Item/AbsorbPotion.PNG"
    AbsorbPotionLow = "Images/Item/AbsorbPotionLow.PNG"
    AdamantPlatebody = "Images/Item/AdamantPlatebody.PNG"
    BowNote = "Images/Item/BowNote.PNG"
    Capture = "Images/Item/Capture.PNG"
    Hammer = "Images/Item/Hammer.PNG"
    Orb = "Images/Item/Orb.PNG"
    Plank = "Images/Item/Plank.PNG"
    PlankNote = "Images/Item/PlankNote.PNG"
    Shark = "Images/Item/Shark.PNG"
    Staff = "Images/Item/Staff.PNG"
    StaffNote = "Images/Item/StaffNote.PNG"
    TeleportHouse = "Images/Item/TeleportHouse.PNG"

class Option(Enum):
    Continue = "Images/Option/Continue.PNG"
    Exchange = "Images/Option/Exchange.PNG"
    Fetch = "Images/Option/Fetch.PNG"
    Mahogany = "Images/Option/Mahogany.PNG"
    YesNo = "Images/Option/YesNo.PNG"

class Status(Enum):
    HealthMedium = "Images/Status/HealthMedium.PNG"