import pyautogui
import time
import random

def type_keys(keys, interval=0.05):
    """
    Types a string of characters with a random delay between each keypress.
    """
    for char in keys:
        pyautogui.press(char)
        time.sleep(random.uniform(interval - 0.02, interval + 0.02))

def delayed_key_press(key, delay_range=(.5, 1.0)):
    """
    Presses a key after a random delay.
    """
    time.sleep(random.uniform(*delay_range))
    pyautogui.press(key)

def random_hotkey_combination(hotkeys, interval=0.05):
    """
    Performs a random combination of hotkeys.
    """
    for hotkey in hotkeys:
        pyautogui.hotkey(*hotkey)
        time.sleep(random.uniform(interval - 0.02, interval + 0.02))

def hold_key(key, duration):
    """
    Holds down a key for a set duration.
    """
    pyautogui.keyDown(key)
    time.sleep(duration)
    pyautogui.keyUp(key)
