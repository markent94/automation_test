from dataclasses import dataclass
import cv2
import numpy as np
from mss import mss
import os
import random 
import Utility.cursor as Cursor
import Utility.keyboard as Keyboard
from Utility.utils import hex_to_bgr
import time
from enum import Enum
import pkg_resources
from Config.config import ROI
from Utility.file_mapping import *

def draw_target_on_screen(target_bounds, ui = ROI.FULL, attempts = 3):      
    with mss() as sct:
        screenshot = sct.grab(ui.value)
        img = np.array(screenshot)
        screenshot = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

    for bound in target_bounds:
        top_left, bottom_right = bound
        cv2.rectangle(screenshot, top_left, bottom_right, (0, 255, 0), 2)  # Draw a Green rectangle

    cv2.imshow('Detected targets', screenshot)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
def find_target_on_screen(path, ui = ROI.FULL, attempts = 3, threshold = .75):   
    target_image_path = path.value
    # if color
    #print(f'Looking for {target_image_path}')
    if '#' in target_image_path:
        return find_color_on_screen(target_image_path, ui = ui)
    
    with mss() as sct:
        screenshot = sct.grab(ui.value)
        img = np.array(screenshot)
        screenshot = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    full_path = pkg_resources.resource_filename('Utility', target_image_path)
    target_image = cv2.imread(full_path, cv2.IMREAD_UNCHANGED)
    if target_image is None:
        print(f"Failed to load the image: {full_path}")
        return None

    if target_image.shape[-1] == 4:
        target_image = target_image[:, :, :3]
    screenshot_gray = cv2.cvtColor(screenshot, cv2.COLOR_BGR2GRAY)
    target_image_gray = cv2.cvtColor(target_image, cv2.COLOR_BGR2GRAY)

    # Get dimensions of the target image
    target_height, target_width = target_image_gray.shape[:2]

    # Perform template matching
    result = cv2.matchTemplate(screenshot_gray, target_image_gray, cv2.TM_CCOEFF_NORMED)

    # Define a ui_options.threshold
    locations = np.where(result >= (threshold))

    # Find the Cursor.position and bounds of all matches
    target_bounds = []
    for pt in zip(*locations[::-1]):  # Switch x and y locations
        x = ui.value['left'] + pt[0]
        y = ui.value['top'] + pt[1]
        top_left = (x, y)
        bottom_right = (x + target_width, y + target_height)
        target_bounds.append((top_left, bottom_right))  # Append the bounds (top-left and bottom-right)

    attempts -= 1
    if attempts  <= 0:
        return target_bounds
        
    # Move cursor and try again in case it is blocked by cursor
    if not target_bounds:
        x, y = Cursor.position()
        Cursor.nudge(x, y)
        threshold -= .05
        target_bounds = find_target_on_screen(path, ui , threshold, attempts)

    return target_bounds 
    
def find_color_on_screen(target_color, ui = ROI.FULL, attempts = 2):
    if ui == ROI.FULL:
        ui = ROI.GAME     

    # Take a screenshot
    with mss() as sct:
        screenshot = sct.grab(ui.value)
        img = np.array(screenshot)
        hsv_screenshot = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    # Convert screenshot to HSV
    #hsv_screenshot = cv2.cvtColor(screenshot, cv2.COLOR_BGR2HSV)

    # Convert the target hex color to BGR, then to HSV
    target_bgr = hex_to_bgr(target_color)
    target_hsv = cv2.cvtColor(np.uint8([[target_bgr]]), cv2.COLOR_BGR2HSV)[0][0]
    
    # Define a range for the color
    sensitivity = 5
    lower_color = np.array([target_hsv[0] - sensitivity, max(target_hsv[1] - sensitivity, 0), max(target_hsv[2] - sensitivity, 0)])
    upper_color = np.array([target_hsv[0] + sensitivity, min(target_hsv[1] + sensitivity, 255), min(target_hsv[2] + sensitivity, 255)])

    # ui_options.threshold the HSV image to get only the target colors
    mask = cv2.inRange(hsv_screenshot, lower_color, upper_color)

    # Find contours and get the largest one
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # Create an array to hold the bounds inside the colored areas
    target_bounds = []

    # Iterate through all the contours
    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        # Create a more refined bounding box if necessary
        # For example, shrink the bounding box slightly to be inside the colored area
        # This adjustment depends on how much you want to shrink the bounding box
        padding = 2  # Adjust padding as needed
        x += ui.value['left']
        y += ui.value['top']
        target_bounds.append(((x + padding, y + padding), (x + w - padding, y + h - padding)))
    
    attempts -= 1
    if attempts <= 0:
        return target_bounds
        
    # Move cursor and try again in case it is blocked by cursor
    if not target_bounds:
        print("Rotating to look for color")
        Cursor.rotate_view()
        target_bounds = find_color_on_screen(target_color, ui, attempts)
 
    return target_bounds

def click_target(target, mouse_button = 'left', ui = ROI.FULL, wait = False): 
    if wait:
        target_bounds = wait_until_target(target, ui = ui)
    else:
        target_bounds = find_target_on_screen(target, ui = ui)
    if target_bounds:
        Cursor.safe_click(target_bounds, mouse_button = mouse_button)
        return True
    return False

def interact_with_target(target, interaction, ui = ROI.FULL, wait = False):
    # Special case for interaction since target might move
    for i in range(3):
        if click_target(target, mouse_button = 'right', ui = ui, wait = wait):
            if click_target(interaction, mouse_button = 'left', ui = ROI.FULL):
                return True
    return False

def press_key_on_target_found(target, keyboard_button, ui = ROI.FULL, wait = True):
    if wait:
        target_bounds = wait_until_target(target, ui)
    else:
        target_bounds = find_target_on_screen(target, ui)
    
    if target_bounds:
        Keyboard.delayed_key_press(keyboard_button)
        
def wait_until_target(target, ui = ROI.FULL, wait_count = 10, delta = 1.0):
    target_options = []
    while wait_count >= 0:
        wait_count -= 1
        time.sleep(delta)
        target_options = find_target_on_screen(target, ui)
        if target_options:
            return target_options
    return target_options

def toggle_menu(target):         
    if target == Button.Bag:
        Keyboard.delayed_key_press('F1')
        #print(f'Toggled Menu: {target}')
        return True
    elif target == Button.Magic:
        Keyboard.delayed_key_press('F3')
        #print(f'Toggled Menu: {target}')
        return True
    elif target == Button.Exit:
        Keyboard.delayed_key_press('F12')
        #print(f'Toggled Menu: {target}')
        return True
        
    target_bounds = find_target_on_screen(target)
    if target_bounds:
        Cursor.safe_click(target_bounds)
        print(f'Toggled Menu: {target}')
        return True
    print(f'{target} Menu not found at {target}')
    return False
    
def set_focus_to_game():
    Cursor.move_in_game_screen()
    Cursor.delayed_click('right')
    
def inventory_count(target):
    toggle_menu(Button.Bag)
    target_bounds = find_target_on_screen(target)
    if target_bounds:
        return len(target_bounds)
    return 0