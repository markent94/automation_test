import random
import threading
import time

from Config.config import *

class Queue:
    def __init__(self):
        self.action_queue = []

    def run_next_action(self):
        if self.action_queue:
            current_action = self.action_queue.pop(0)
            return run(current_action)         
        else:
            return True
        return True

    def add_action_to_queue(self, action):
        self.action_queue.append(action)
        #print(f"Added {action} to queue.")


class TimedQueue:
    def __init__(self, action_timers, initial_action = ACTION.NONE):
        self.action_timers = action_timers
        self.action_queue = []
        if initial_action != ACTION.NONE:
            self.add_action_to_queue(initial_action)
        self.initialize_action_timers()

    def run_next_action(self):
        if self.action_queue:
            current_action = self.action_queue.pop(0)
            action_method = get_action_method(current_action)
            if action_method:
                if action_method():
                    # Reset the action timer for the popped action
                    self.action_timer(current_action, self.action_timers[current_action])
                    return True
                else:
                    print(f"Action: {current_action} failed, adding to back of queue")
                    self.add_action_to_queue(current_action)
                    return False              
        else:
            print("No action in queue, defaulting to True.")
            time.sleep(1)
        return True

    def add_action_to_queue(self, action):
        self.action_queue.append(action)
        #print(f"Added {action} to queue.")

    def action_timer(self, action, base_duration):
        # Calculate a new duration within a ±15% range of the base duration
        variation = base_duration * 0.15  # 15% of the base duration
        new_duration = random.uniform(base_duration - variation, base_duration + variation)

        #print(f"Timer started for {action}, lasting {new_duration} seconds.")
        threading.Timer(new_duration, self.add_action_to_queue, [action]).start()

    def initialize_action_timers(self):
        for action, duration in self.action_timers.items():
            self.action_timer(action, duration)
