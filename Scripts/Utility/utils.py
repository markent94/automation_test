import numpy as np

# Function to calculate the Euclidean distance between two points
def calculate_distance(x1, y1, x2, y2):
    return ((x2 - x1)**2 + (y2 - y1)**2)**0.5
    
def quadratic_bezier(t, start, control, end):
    t = np.array(t)  # Ensure t is an array for efficient calculations
    return (1 - t)**2 * np.array(start) + 2 * (1 - t) * t * np.array(control) + t**2 * np.array(end)

def clamp(value, min_value, max_value):
    return np.clip(value, min_value, max_value)
    
# Convert hex color to BGR
def hex_to_bgr(hex_color):
    hex_color = hex_color.lstrip('#')
    lv = len(hex_color)
    return tuple(int(hex_color[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))[::-1]

def time_string_to_seconds(time_string):
    # Split the time string into parts
    parts = time_string.split(':')

    # Calculate the total seconds depending on the number of parts
    if len(parts) == 2:  # Format is MM:SS
        minutes, seconds = parts
        total_seconds = int(minutes) * 60 + int(seconds)
    elif len(parts) == 3:  # Format is HH:MM:SS
        hours, minutes, seconds = parts
        total_seconds = int(hours) * 3600 + int(minutes) * 60 + int(seconds)
    else:
        raise ValueError("Time string format should be MM:SS or HH:MM:SS")

    return total_seconds

