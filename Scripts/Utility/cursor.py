import pyautogui
import time
import random
from Utility.utils import calculate_distance, quadratic_bezier, clamp
from Config.config import *

SCREEN_WIDTH, SCREEN_HEIGHT = pyautogui.size()

MARGIN = 250 # pixels from the edge of the screen
FPS = 30.0

def move_to(end_x, end_y):
    time.sleep(random.uniform(0.01, 0.05))
    safe_end_x = max(MARGIN, min(end_x, SCREEN_WIDTH - MARGIN))
    # Small MARGIN at top because the screen is at the top
    safe_end_y = max(MARGIN / 10, min(end_y, SCREEN_HEIGHT - MARGIN))
    
    start_x, start_y = position()
    
    distance = calculate_distance(start_x, start_y, safe_end_x, safe_end_y)

    # Exit early if there's no distance to move
    if distance < 1e-6:
        print("Start and end points are the same. No movement needed.")
        return

    min_speed = 0.00001  # minimum speed (seconds per move)
    max_speed = 0.00005  # maximum speed (seconds per move)
    speed_range = max_speed - min_speed

    control_x = (start_x + end_x) / 2 + random.uniform(-distance/4, distance/4)
    control_y = (start_y + end_y) / 2 + random.uniform(-distance/4, distance/4)
    
    steps = int(distance / FPS)
    steps = clamp(steps, 10, 1000)
    for step in range(steps):
        t = pyautogui.easeInOutQuad(step / float(steps))
        x, y = quadratic_bezier(t, (start_x, start_y), (control_x, control_y), (end_x, end_y))
        step_distance = calculate_distance(x, y, end_x, end_y)

        # Avoid divide by zero error
        if distance > 0:
            duration = (step_distance / distance) * speed_range + min_speed
            pyautogui.moveTo(x, y, duration=duration)

    pyautogui.moveTo(end_x, end_y, min_speed)

def safe_click(target_bounds, mouse_button='left'):
    # Add slight movements to mimic inaccuracies in mouse movement
    choices = [0, 1, 2]
    weights = [0.97, 0.03, 0.01]  # Adjust the weights as desired

    #print(target_bounds)
    if (get_random_point_in_bounds(target_bounds)):
        target_x, target_y = get_random_point_in_bounds(target_bounds)
        # Use random.choices to pick a value based on the weights
        result = random.choices(choices, weights, k=1)[0]
        for i in range(result):
            nudge(target_x, target_y, random.randint(10, 20))
        move_to(target_x, target_y)
    delayed_click(mouse_button)
    
def nudge(target_x, target_y, distance = 50):
    mid_x = random.randint(target_x - distance, target_x + distance)
    mid_y = random.randint(target_y - distance, target_y + distance)
    move_to(mid_x, mid_y)
    
# Add random time before click to mimic human behavior
def delayed_click(mouse_button='left'):
    time.sleep(random.uniform(0.01, 0.05))
    pyautogui.click(button=mouse_button)
    #print("Clicked")
  
def position():
    start_x, start_y = pyautogui.position()
    return start_x, start_y
    
def mouse_down( mouse_button='left'):
    start_x, start_y = position()
    pyautogui.mouseDown(start_x, start_y, mouse_button)

def mouse_up(mouse_button='left'):
    pyautogui.mouseUp(button=mouse_button)
   
def rotate_start():
    move_in_game_screen()
    mouse_down('middle')
    
def rotate_end():
    mouse_up('middle')
    
def move_out_game_screen(direction = 'left'):
    if direction == 'left':
        target_x = random.randint(ROI.FULL.value['left'] + ROI.FULL.value['width'], ROI.FULL.value['left'] + ROI.FULL.value['width'] + 200) 
    else:
        target_x = random.randint(ROI.FULL.value['left'] + ROI.FULL.value['width'] - 200, ROI.FULL.value['left'] + ROI.FULL.value['width'])
    target_y = random.randint(500, 600)
    move_to(target_x, target_y)
   
def rotate_view(direction='left'):
    move_in_game_screen()
    mouse_down('middle')
 
    # Choose the target quadrant based on the direction
    if direction == 'left':
        move_out_game_screen(direction)
    elif direction == 'right':
        move_out_game_screen(direction)
    else:
        print("Invalid direction specified. Choose 'left' or 'right'.")
        return
    mouse_up('middle')
    
def move_in_game_screen():
    target_x = random.randint(ROI.FULL.value['left'], ROI.FULL.value['left'] + ROI.FULL.value['width'])
    target_y = random.randint(ROI.FULL.value['top'], ROI.FULL.value['top'] + ROI.FULL.value['height'])
    move_to(target_x, target_y)
    
def get_random_point_in_bounds(bounds):
    while bounds:  # Continue until bounds is empty
        bound = random.choice(bounds)  # Randomly select a bound
        (x1, y1), (x2, y2) = bound
        
        # If x1 is equal to x2 or y1 is equal to y2, the bound is considered invalid
        if x1 > x2 or y1 > y2:
            bounds.remove(bound)  # Remove the invalid bound
            continue  # Skip the rest of the loop and try with the next bound
        
        (x1, y1), (x2, y2) = reduce_bounds(bound)
        
        # If the bound is valid, return a random point within it
        random_x = random.randint(x1, x2)
        random_y = random.randint(y1, y2)
        return (random_x, random_y)

    print(bounds)
    print("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO")
    # If all bounds were invalid or the list was empty to begin with
    return None
    
def reduce_bounds(bounds, reduction_factor=0.5):
    """
    Reduces the size of given bounds to a specified percentage of its original size.

    :param bounds: A tuple of the form ((x1, y1), (x2, y2)) representing the original bounds.
    :param reduction_factor: The factor by which to reduce the size of the bounds (0.5 for 50%).
    :return: A tuple representing the reduced bounds.
    """
    (x1, y1), (x2, y2) = bounds

    # Calculate the midpoint of the original bounds
    midpoint_x = (x1 + x2) / 2
    midpoint_y = (y1 + y2) / 2

    # Calculate the dimensions of the original bounds
    original_width = x2 - x1
    original_height = y2 - y1

    # Calculate the dimensions of the reduced bounds
    new_width = original_width * reduction_factor
    new_height = original_height * reduction_factor

    # Calculate the new top-left and bottom-right points
    new_x1 = int(midpoint_x - new_width / 2)
    new_y1 = int(midpoint_y - new_height / 2)
    new_x2 = int(midpoint_x + new_width / 2)
    new_y2 = int(midpoint_y + new_height / 2)

    return ((new_x1, new_y1), (new_x2, new_y2))