import pyautogui
from enum import Enum
from Utility.file_mapping import *
import Utility.utils as Utils

# Define your screen's region of interest (ROI)
# x, y is the top left corner, and width, height defines the size of the region
class ROI(Enum):
    FULL = {'top': 25, 'left': 438, 'width': 760, 'height': 500}
    GAME = {'top': 25, 'left': 438, 'width': 954 - 438, 'height': 361 - 25}
    MINIMAP = {'top': 25, 'left': 953, 'width': 1202 - 953, 'height': 186 - 25}
    INVENTORY = {'top': 227,     'left': 985, 'width': 1175 - 985, 'height': 488 - 227}

# Failed consecutive actions for reset and exiting
RESET_THRESHOLD = 10
EXIT_THRESHOLD = 25

SIMULATION_LENGTH = Utils.time_string_to_seconds("15:00:00")
ACTIVE_DURATION = (Utils.time_string_to_seconds("04:30:00"), Utils.time_string_to_seconds("06:00:00"))
BREAK_DURATION = (Utils.time_string_to_seconds("00:30:00"), Utils.time_string_to_seconds("01:30:00"))

# Define or import your STATE Enum
class STATE(Enum):
    IDLE = 1
    BREAK = 2
    ALCHING = 3
    CONSTRUCTING = 4
    CRAFTING = 5
    ATTACKING = 6

class ACTION(Enum):
    ATTACK = 1
    QUICKPRAY = 2
    ABSORB = 3
    BUILD_FURNITURE = 4
    REMOVE_FURNITURE = 5
    FIRE_CANNON = 6
    ALCH = 7
    HEAL = 8
    ENTER_POH = 9
    EXIT_POH = 10
    GET_PLANKS = 11
    RANDOM = 12
    BREAK = 13
    NONE = 14

def action_methods():
    # Deferred import to avoid circular dependency
    import Actions.action_utility as Utility
    import Actions.action_combat as Combat
    import Actions.action_construction as Construction
    import Actions.action_random as Random

    method_dict = {
        ACTION.ALCH: Utility.alch,
        ACTION.ENTER_POH: Utility.enter_house,
        ACTION.EXIT_POH: Utility.exit_house,
        ACTION.BREAK: Utility.perform_break_action,
        ACTION.RANDOM: Random.perform_random_action,
        ACTION.ATTACK: Combat.attack,
        ACTION.FIRE_CANNON: Combat.fire_cannon,
        ACTION.QUICKPRAY: Combat.quick_pray,
        ACTION.ABSORB: Combat.absorb,
        ACTION.HEAL: Combat.heal,
        ACTION.BUILD_FURNITURE: Construction.build_furniture,
        ACTION.REMOVE_FURNITURE: Construction.remove_furniture,
        ACTION.GET_PLANKS: Construction.get_planks
    }
    return method_dict

def get_action_method(action):
    method = action_methods().get(action, None)
    if method:
        return method
    print(f"No method defined for {action}")
    return 

def run(action):
    method = get_action_method(action)
    if method:
        return method()
    return False

#ACTION_TIMERS = {
#    ACTION.ATTACK: 60,
#    ACTION.PRAY: 23,
#    ACTION.ABSORB: 180,
#    ACTION.FIRE: 12,
#    ACTION.ALCH: 1
#}

# Profile for Slayer
#COMBAT_TIMERS = {
#    ACTION.ATTACK: 25,
#    ACTION.HEAL: 0
#}

# Profile for Experiments
COMBAT_TIMERS = {
    ACTION.ATTACK: 40,
    ACTION.ALCH: 0
}

MAIN_STATE = STATE.ATTACKING
ADDITIONAL_STATES = {}

ALCHEMY_TARGETS = {Item.BowNote, Item.StaffNote}


