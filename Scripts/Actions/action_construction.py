import Utility.ui_helper as UI
from Config.config import *

class ActionConstruction:
    def __init__(self):
        self.previous_action = ACTION.NONE

    def perform_action(self):
        if UI.inventory_count(Item.Plank) >= 4:
            if run(ACTION.ENTER_POH):
                if self.previous_action in [ACTION.BUILD_FURNITURE, ACTION.NONE]:
                    if run(ACTION.REMOVE_FURNITURE):
                        self.previous_action = ACTION.REMOVE_FURNITURE
                        return True
                    self.previous_action = ACTION.NONE
                if self.previous_action in [ACTION.REMOVE_FURNITURE, ACTION.NONE]:
                    if run(ACTION.BUILD_FURNITURE):
                        self.previous_action = ACTION.BUILD_FURNITURE
                        return True
                    self.previous_action = ACTION.NONE
        else:
            if run(ACTION.EXIT_POH):
                if run(ACTION.GET_PLANKS):
                    if run(ACTION.ENTER_POH):
                        return True
        print("ActionConstruction failed")
        return False

def get_planks():
    print("LOOKING FOR BLUE")
    if UI.find_target_on_screen(Color.Blue):
        if UI.click_target(Item.PlankNote):
            if UI.click_target(Color.Blue):
                if UI.press_key_on_target_found(Option.Exchange, '3'):
                    return True
    print("get_planks failed")
    return False

def remove_furniture():
    print("LOOKING FOR GREEN")
    if UI.interact_with_target(Color.Green, Interaction.Remove):
        if UI.press_key_on_target_found(Option.YesNo, '1'):
            if UI.wait_until_target(Color.Pink):
                return True
            elif UI.press_key_on_target_found(Option.YesNo, '1'):
                if UI.wait_until_target(Color.Pink):
                    return True
    print("remove_furniture failed")
    return False

def build_furniture():
    print("LOOKING FOR PINK")
    if UI.interact_with_target(Color.Pink, Interaction.Build):
        if UI.press_key_on_target_found(Option.Mahogany, '6'):
            if UI.wait_until_target(Color.Green):
                return True
    print("build_furniture failed")
    return False