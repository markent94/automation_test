import sys
import random
import Utility.cursor as Cursor
import Utility.ui_helper as UI
import Utility.keyboard  as Keyboard
import time
from Config.config import *
from Utility.file_mapping import *


def perform_break_action():
    finished_break = False
    attempts_remaining = 100
    while attempts_remaining >= 0:
        if not UI.find_target_on_screen(Button.PlayNow):
            click_logout_button()

        if UI.find_target_on_screen(Button.PlayNow) or UI.find_target_on_screen(Button.Play):
            if finished_break:
                if perform_login_action():
                    return True    
            else:
                break_ticks = random.uniform(*BREAK_DURATION)
                print(f"BREAKING FOR {break_ticks} SECONDS")
                while break_ticks > 0:
                    print(f'{break_ticks} seconds left on break')
                    break_ticks -= 1
                    time.sleep(1)
                finished_break = True
                print('BREAK DONE')
        else:
            print("Not logged out, might be in combat")
            time.sleep(random.uniform(0.5, 1.5))
        attempts_remaining -= 1

    print("ERROR: ActionBreak failed - EXITING")
    sys.exit(0)

def alch():
    target_list = ALCHEMY_TARGETS
    UI.toggle_menu(Button.Magic)
    print("starting alch")
    if UI.click_target(Button.HighAlchemy):
        print("clicking alch")
        if isinstance(target_list, set):
            target_list = list(target_list)
        if UI.click_target(random.choice(target_list), ui = ROI.INVENTORY):
            return True
    print("alch failed")
    return False
            
def perform_login_action():
    if UI.click_target(Button.Play, wait = True):
        return True
    UI.click_target(Button.PlayNow)
    print("perform_login_action failed")
    return False

def click_logout_button():
    UI.toggle_menu(Button.Exit)
    if UI.click_target(Button.Logout):
        return True
    print("click_logout_button failed")
    return False

def teleport_to_house():
    UI.toggle_menu(Button.Bag)
    if click_teleport_house_button():
        return True
    print("teleport_to_house failed")
    return False        
    
def click_teleport_house_button():
    if UI.click_target(Item.TeleportHouse, ui = ROI.INVENTORY):
        print("Teleporting to house")
        time.sleep(3)
        return True
    print("click_teleport_house_button failed")
    return False
        
def move_tile():
    print("Moving to tile")
    tile = UI.find_target_on_screen(Environment.TileMarker, threshold = .7)
    if tile:
        Cursor.safe_click(tile)
        time.sleep(random.uniform(5, 7))
        return True
    print("Move tile failed")
    return False

def enter_house():
    print("Entering house")
    if UI.find_target_on_screen(Color.Purple):
        return True
    if UI.interact_with_target(Color.Yellow, Interaction.BuildMode):
        if UI.wait_until_target(Color.Purple):
            return True
    print("enter_house failed")
    return False

def exit_house():
    print("Exiting house")
    if UI.find_target_on_screen(Color.Yellow):
        return True
    if UI.click_target(Color.Purple):
        if UI.wait_until_target(Color.Yellow):
            return True
    print("exit_house failed")
    return False

