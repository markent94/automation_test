import random
import Utility.cursor as Cursor
import Utility.ui_helper as UI
import time
from Config.config import *
from Utility.file_mapping import *

MOUSE_MOVEMENT_PROBABILITY = 0.1
LONG_PAUSE_PROBABILITY = 0.01
MIDDLE_MOUSE_MOVEMENT_PROBABILITY = 0.01
RIGHT_CLICK_PROBABILITY = 0.01

def perform_random_action():
    random_mouse_movement(MOUSE_MOVEMENT_PROBABILITY)
    random_long_pause(LONG_PAUSE_PROBABILITY)
    random_rotate(LONG_PAUSE_PROBABILITY)
    random_right_click(RIGHT_CLICK_PROBABILITY)
    return True

def random_right_click(probability):
    random_value = random.random()
    if random_value > probability:
        return False
        
    print("Random Right Click")
    UI.set_focus_to_game()
    
def random_mouse_movement(probability):
    random_value = random.random()
    if random_value > probability:
        return False
        
    print("Random Movement")
    # Generate a random target Cursor.position
    target_x = random.randint(500, Cursor.SCREEN_WIDTH - 500)
    target_y = random.randint(500, Cursor.SCREEN_HEIGHT - 500)
    Cursor.move_to(target_x, target_y)
    return True
    
def random_rotate(probability):
    random_value = random.random()
    if random_value > probability:
        return False
        
    print("Random Rotate")  
    Cursor.rotate_start()
    count = random.randint(1,5)
    for i in range(count):
        random_mouse_movement(1.0)
    Cursor.rotate_end()

def random_long_pause(probability, min_length = 5, max_length = 10):
    random_value = random.random()
    if random_value > probability:
        return False
    
    pause_length = random.uniform(min_length, max_length)
    print(f'Long Pause: {pause_length}')
    time.sleep(pause_length)
    return True