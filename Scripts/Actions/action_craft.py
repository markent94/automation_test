import random
import time
import Utility.cursor as Cursor
import Utility.ui_helper as UI
import Utility.keyboard as Keyboard
from Config.config import *
from Utility.file_mapping import *

class ActionCraft:
    def __init__(self):
        pass

    def perform_action(self):
        if self.get_materials():
            if self.combine_materials():
                time.sleep(random.uniform(17, 20))
                if self.deposit_materials():
                    return True
        self.handle_failure()
        print("ActionCraft failed")
        return False     

    def get_materials(self):
        bank = UI.find_target_on_screen(Button.DepositBag)
        if not bank:
            print("LOOKING FOR BLUE")
            UI.click_target(Color.Blue)  # Using the Color enum
        if UI.click_target(Item.Orb, wait=True) and UI.click_target(Item.Staff):
            time.sleep(random.uniform(0.75, 1))
            Keyboard.delayed_key_press('escape')
            return True
        print("get_materials failed")
        return False
    
    def combine_materials(self):  
        if UI.click_target(Item.Orb) and UI.click_target(Item.Staff):
            time.sleep(random.uniform(0.5, 1.0))
            Keyboard.delayed_key_press('space')
            return True
        print("combine_materials failed")
        return False
    
    def deposit_materials(self):    
        print("LOOKING FOR BLUE")
        if UI.click_target(Color.Blue) and UI.click_target(Button.DepositBag, wait=True):
            return True
        print("deposit_materials failed")
        return False
    
    def handle_failure(self):    
        Keyboard.delayed_key_press('escape')
        if self.deposit_materials():
            time.sleep(random.uniform(0.75, 1))
            Keyboard.delayed_key_press('escape')