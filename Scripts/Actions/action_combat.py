import time
import Utility.cursor as Cursor
import random
import Utility.ui_helper as UI
from Actions.action_utility import move_tile
from Config.config import *
from Utility.action_queue import TimedQueue

class ActionCombat:
    def __init__(self):
        #Initial scrap sum faka
        self.queue = TimedQueue(action_timers = COMBAT_TIMERS)

    def perform_action(self):
        return self.queue.run_next_action()
    
def attack():
    print("ATTACKING PINK")
    if UI.interact_with_target(Color.Pink, Interaction.Attack):
        time.sleep(random.uniform(4, 6))
        return True
    move_tile
    print("Random attack failed")
    return False

def heal():
    if UI.find_target_on_screen(Status.HealthMedium, attempts=1, threshold=0.87):
        print("Healing")
        UI.toggle_menu(Button.Bag)
        if UI.click_target(Item.Shark):
            return True
    time.sleep(1)
    return True

def absorb():
    UI.toggle_menu(Button.Bag)
    if UI.click_target(Item.AbsorbPotion):
        return True
    if UI.click_target(Item.AbsorbPotionLow):
        return True
    return False

def fire_cannon():
    if UI.interact_with_target(Color.Yellow, Interaction.Fire):
        return True
    return False

def quick_pray():
    if UI.click_target(Button.QuickPray, ui = ROI.MINIMAP):
        time.sleep(.25)
        Cursor.delayed_click()
        return True
    print("Quick pray failed")
    return False 